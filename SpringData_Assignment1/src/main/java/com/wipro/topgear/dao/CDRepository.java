package com.wipro.topgear.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.wipro.topgear.model.CDBean;

@Repository
public interface CDRepository extends CrudRepository<CDBean, Long> {

	List<CDBean> findByCdtitle(String cdtitle);
	
	List<CDBean> findByCdpublisher(String cdpublisher);
	
	
}
