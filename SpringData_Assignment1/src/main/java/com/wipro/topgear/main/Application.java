package com.wipro.topgear.main;
import static java.lang.System.exit;

import java.text.SimpleDateFormat;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.Transactional;

import com.wipro.topgear.dao.CDRepository;
import com.wipro.topgear.model.CDBean;

/*
1.	Create a Spring Data application which maintains the below entity.

Entity Name	CD	
Properties:		
	cdid		Long
	cdtitle		String
	cdprice		Float
	cdpublisher	String
	
	Requirements that needs to be implemented in the application:
a.	Use import.sql file or add sample records in the respective table.
b.	The application should allow me to find all the CD details with all the properties
c.	The application should also allow me to query specific CD/a group of CDs either by its cdtitle or cdpublisher



*/
@SpringBootApplication
@EnableJpaRepositories("com.wipro.topgear.dao")
@EntityScan(basePackages="com.wipro.topgear.model")
public class Application implements CommandLineRunner {

    @Autowired
    DataSource dataSource;

    @Autowired
    CDRepository cdRepository;

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
    }

    @Transactional(readOnly = true)
    public void run(String... args) throws Exception {

        System.out.println("DATASOURCE = " + dataSource);

        // to fetch all the CDs(details) which are our the database
        getAllCds();
        
        // to get all the CD details by title name
        getCDDetailsByTitle("Lina Blues");
        
        // to get all the CD details by publisher name
        getCDDetailsByPublisher("Jaboo Smith");
        
       

        System.out.println("Successfully executed all the operations!");

    }
    
    private void getCDDetailsByTitle(String title) {
    	
    	
    	System.out.println("\n1.findByTitle(String title)...");
        List<CDBean> listOfCDsByTitle= (List<CDBean>)cdRepository.findByCdtitle("Lina Blues");
        if(listOfCDsByTitle.size()>0) 
        {
        for(CDBean cd:listOfCDsByTitle)
        System.out.println(cd);
        }
        else {
        	System.out.println("Sorry!!No CD exists with this title");
        }
    	
    }
    
    
    
    
    private void getCDDetailsByPublisher(String publisher) {
    	
    	 System.out.println("\n1.findByPublisher(String publisher)...");
         List<CDBean> listOfCDsByPublisher= (List<CDBean>)cdRepository.findByCdpublisher("Jaboo Smith");
         if(listOfCDsByPublisher.size()>0) 
         {
         for(CDBean cd:listOfCDsByPublisher)
         System.out.println(cd);
         }
         else {
         	System.out.println("No CD exists with this publisher name");
         }
    	
    	
    	
    }
    
    
    private void getAllCds() {
    	
    	System.out.println("\n1.findAll()...");
        List<CDBean> listOfCDs= (List<CDBean>)cdRepository.findAll();
        if(listOfCDs.size()>0) 
        {
        for(CDBean cd:listOfCDs)
        System.out.println(cd);
        }
        else {
        	System.out.println("No CD exists in our database");
        }

    	
    	
    }
    
    
    
    
    
    
    
    

}