package com.wipro.topgear.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="cdbean")
public class CDBean {

	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CD_SEQ")
    @SequenceGenerator(sequenceName = "cd_seq", allocationSize = 1, name = "CD_SEQ")
	private  int cdid;
	
	private  String cdtitle;
	 
	private Float cdprice;
	 
	private String cdpublisher;

	public CDBean() {
		super();
	}
	
	public CDBean(int cdid, String cdtitle, Float cdprice, String cdpublisher) {
		super();
		this.cdid = cdid;
		this.cdtitle = cdtitle;
		this.cdprice = cdprice;
		this.cdpublisher = cdpublisher;
	}

	public int getId() {
		return cdid;
	}

	public void setId(int cdid) {
		this.cdid = cdid;
	}

	public String getCdtitle() {
		return cdtitle;
	}

	public void setCdtitle(String cdtitle) {
		this.cdtitle = cdtitle;
	}

	public Float getCdprice() {
		return cdprice;
	}

	public void setCdprice(Float cdprice) {
		this.cdprice = cdprice;
	}

	public String getCdpublisher() {
		return cdpublisher;
	}

	public void setCdpublisher(String cdpublisher) {
		this.cdpublisher = cdpublisher;
	}

	@Override
	public String toString() {
		return "CDBean [cdid=" + cdid + ", cdtitle=" + cdtitle + ", cdprice=" + cdprice + ", cdpublisher=" + cdpublisher
				+ "]";
	}

}
