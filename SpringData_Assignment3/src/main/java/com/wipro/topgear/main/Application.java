package com.wipro.topgear.main;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.wipro.topgear.dao.CDRepository;
import com.wipro.topgear.model.CDBean;
/*
3.	Rewrite the above solution of assignment (2) with the below updates/additions:

a.	Use the appropriate Transactions decorator/annotation (@Transactional) and the �readOnly� property as well if its applicable
b.	Use the timeout, rollbackFor transactional  properties as well wherever applicable
c.	Use the @Modifying annotation wherever applicable
*/




@SpringBootApplication
@EnableJpaRepositories("com.wipro.topgear.dao")
@EntityScan(basePackages = "com.wipro.topgear.model")
public class Application implements CommandLineRunner {

	@Autowired
	DataSource dataSource;

	@Autowired
	CDRepository cdRepository;

	public static void main(String[] args) throws Exception {
		SpringApplication.run(Application.class, args);
	}

	@Transactional(readOnly = true)
	public void run(String... args) throws Exception {

		System.out.println("DATASOURCE = " + dataSource);

		
			//to  insert a new CD in the database
		CDBean cdBean = new CDBean(1, "Little Willie Blues", 786.0f, "Jaboo Smith");
		saveCDDetails(cdBean);
		
		//to update  CD details in the database
		//updateCDDetails(1, "Black Snake Blues", 1272.0f, "Spivey and johnson");
		
		//deleteCD(1);
		
		// to get the CD details by id
		getaCdById(1);

		// to fetch all the CDs(details) which are in our the database
		// getAllCds();

		// to fetch the CD details by title
		// getCDDetailsByTitle("Lina Blues");

		// to fetch CD details by publisher name
		// getCDDetailsByPublisher("Jaboo Smith");

		System.out.println("Successfully executed all the operations!");

	}

	@Transactional(readOnly=true,			// readOnly if set to true means updation is not possible.
			timeout=50)    			                // Method to be executed with a timeout of 50 seconds
	private void getCDDetailsByTitle(String title) {

		System.out.println("\n1.findByTitle(String title)...");
		List<CDBean> listOfCDsByTitle = (List<CDBean>) cdRepository.findByCdtitle("Lina Blues");
		if (listOfCDsByTitle.size() > 0) {
			for (CDBean cd : listOfCDsByTitle)
				System.out.println(cd);
		} else {
			System.out.println("Sorry!!No CD exists with this title");
		}

	}
	
	@Transactional(readOnly=true,			// readOnly if set to true means updation is not possible.
			timeout=50)    			                // Method to be executed with a timeout of 50 seconds
	private void getCDDetailsByPublisher(String publisher) {

		System.out.println("\n1.findByPublisher(String publisher)...");
		List<CDBean> listOfCDsByPublisher = (List<CDBean>) cdRepository.findByCdpublisher("Jaboo Smith");
		if (listOfCDsByPublisher.size() > 0) {
			for (CDBean cd : listOfCDsByPublisher)
				System.out.println(cd);
		} else {
			System.out.println("No CD exists with this publisher name");
		}

	}
	
	@Transactional(readOnly=true,			// readOnly if set to true means updation is not possible.
			timeout=50)    			        // Method to be executed with a timeout of 50 seconds
	private void getAllCds() {

		System.out.println("\n1.findAll()...");
		List<CDBean> listOfCDs = (List<CDBean>) cdRepository.findAll();
		if (listOfCDs.size() > 0) {
			for (CDBean cd : listOfCDs)
				System.out.println(cd);
		} else {
			System.out.println("No CD exists in our database");
		}
	}

	@Transactional(timeout=10,							// method will be executed with a timeout of 10 seconds.
			   rollbackForClassName="Exception")        // force a DB to rollback if any exception of type java.lang.Exception is thrown by method.  
	private void saveCDDetails(CDBean bean) {

		System.out.println("Saving the record.............");
		CDBean cdbean = cdRepository.save(bean);
		if (cdbean == null) {
			System.out.println("Failed to save the record");
		} else {
			System.out.println("Record saved successfully");
		}
	}

	@Modifying(clearAutomatically=true)					// It will clear the underlying persistence context after executing the modifying query.
	@Transactional(timeout=20,							// Method to be executed with a timeout of 20 seconds
					rollbackForClassName="Exception") 	// force a DB to rollback if any exception of type java.lang.Exception is thrown by method.
	private void updateCDDetails(int cdid, String cdtitle, float cdprice, String cdpublisher) {

		System.out.println("Updating the record............");
		CDBean updatedCDDetail = cdRepository.findOne(cdid);
		updatedCDDetail.setCdprice(cdprice);
		updatedCDDetail.setCdtitle(cdtitle);
		updatedCDDetail.setCdpublisher(cdpublisher);

		CDBean newCD = cdRepository.save(updatedCDDetail);
		System.out.println("Updated CD details are:" + newCD);
	}
	
	
	@Modifying(clearAutomatically=true)					// Clear the underlying persistence context after executing the modifying query. 
	@Transactional(timeout=5,							// Method to be executed with a timeout of 5 seconds
					rollbackForClassName="Exception") 	// force a DB to rollback if any exception of type java.lang.Exception is thrown by method.
	private void deleteCD(int cdid) {

		System.out.println("Record is deleting...................");
		cdRepository.delete(cdid);
		System.out.println("Record deleted sucessfully");
	}

	
	@Transactional(readOnly=true,			// readOnly if set to true means updation is not possible.
	timeout=30)    			                // Method to be executed with a timeout of 30 seconds
	private void getaCdById(int cdid) {
		System.out.println("Fetching a record.............................");
		CDBean cd = cdRepository.findOne(cdid);
		System.out.println("CD Details of a particular Id:" + cd);
	}

}
