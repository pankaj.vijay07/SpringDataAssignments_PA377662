package com.wipro.topgear.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.wipro.topgear.model.CDBean;

@Repository
public interface CDRepository extends CrudRepository<CDBean, Integer> {

@Query("select t from CDBean t where t.cdprice between :price1 AND :price2")	
public List<CDBean> findByPriceRange(@Param("price1") float price1,@Param("price2") float price2);
	
public List<CDBean> findByPriceRangeNamedParams(@Param("price1") float price1,@Param("price2") float price2);

public List<CDBean> findByCdtitle(@Param("cdtitle") String cdtitle);

public List<CDBean> findByTitleNative(@Param("cdtitle") String cdtitle);

public List<CDBean> findAllSorted();
}
