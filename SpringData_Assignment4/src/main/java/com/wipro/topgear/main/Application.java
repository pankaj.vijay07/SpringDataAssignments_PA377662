package com.wipro.topgear.main;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.wipro.topgear.dao.CDRepository;
import com.wipro.topgear.model.CDBean;
/*

4.	Rewrite the solution of assignment (2) with the below updates/additions:
a.	Add the below additional queries :
a.	findByPriceRange ? this query should take a range of �cdprice� and return the CD details within that range. (Use query params)
b.	findByPriceRangeNamedParams ? this query should take a range of �cdprice� and return the CD details within that range (use Named query params)
c.	findByTitleMatch ? this query should return the CD based on the title (use the �like� clause)
d.	findByTitleNative? this query should return the CD based on the title (use the �native query�)
e.	findAllSorted ? this query should return all the CDs but arranged in the given sorted order (Try 2 sorting scenarios: sort it in the ascending order of cdtitle and sort it in the descending order of cdprice)
*/





@SpringBootApplication
@EnableJpaRepositories("com.wipro.topgear.dao")
@EntityScan(basePackages = "com.wipro.topgear.model")
public class Application implements CommandLineRunner {

	@Autowired
	DataSource dataSource;

	@Autowired
	CDRepository cdRepository;

	public static void main(String[] args) throws Exception {
		SpringApplication.run(Application.class, args);
	}

	
	public void run(String... args) throws Exception {

		System.out.println("DATASOURCE = " + dataSource);

		
			//to  insert a new CD in the database
		CDBean cdBean = new CDBean(1, "Little Willie Blues", 786.0f, "Jaboo Smith");
		saveCDDetails(cdBean);
		CDBean cdBean1 = new CDBean(2, "Black Snake Blues", 248.0f, "Spivey and johnson");
		saveCDDetails(cdBean1);
		
		findCDByPriceRange(500.0f, 1000.0f);
		findCDByPriceRangeNamedParams(100.0f, 300.0f);
		findByTitleMatch("Black Snake Blues");
		findAllSorted();
		
		
		System.out.println("Successfully executed all the operations!");

	}

	@Transactional(timeout=10,							// method will be executed with a timeout of 10 seconds.
			   rollbackForClassName="Exception")        // force a DB to rollback if any exception of type java.lang.Exception is thrown by method.  
	private void saveCDDetails(CDBean bean) {

		System.out.println("Saving the record.............");
		CDBean cdbean = cdRepository.save(bean);
		if (cdbean == null) {
			System.out.println("Failed to save the record");
		} else {
			System.out.println("Record saved successfully");
		}
	}
	
	
		private void findCDByPriceRange(float price1, float price2) {
		
		System.out.println("Fetching records...................................");
		List<CDBean> list=cdRepository.findByPriceRange(price1, price2);
		for(CDBean b:list) {
			System.out.println("Fetching records by satisfying price range "+b);
		
	}
		}

	
	
	private void findCDByPriceRangeNamedParams(float price1, float price2) {

		System.out.println("Fetching records...................................");
		List<CDBean> list=cdRepository.findByPriceRangeNamedParams(price1, price2);
		for(CDBean b:list) {
			System.out.println("Fetching records by satisfying price range "+b);
		
	}
	}

	private void findByTitleMatch(String title) {

		List<CDBean> list=cdRepository.findByCdtitle(title);
		for(CDBean b:list) {
			System.out.println("Fetching records by title "+b);
	}
	}

	private void findByTitleNative(String title) {

		System.out.println("Fetching record.............");
		List<CDBean> list=cdRepository.findByTitleNative(title);
		for(CDBean b:list) {
			System.out.println("Fetching records by title using native "+b);
	}
	}

	private void findAllSorted() {

		System.out.println("Fetching record.............");
		List<CDBean> list=cdRepository.findAllSorted();
		for(CDBean b:list) {
			System.out.println("Fetching all records  "+b);
	}
	}
}
