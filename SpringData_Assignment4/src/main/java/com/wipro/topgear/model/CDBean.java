package com.wipro.topgear.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@NamedQueries(
{
@NamedQuery(name = "CDBean.findByPriceRangeNamedParams",
query = "select t from CDBean t where t.cdprice between :price1 AND :price2"
		),

@NamedQuery(name = "CDBean.findByCdtitle",
query = "select t from CDBean t where t.cdtitle like :cdtitle"
		)
}
)
@NamedNativeQueries(
{
@NamedNativeQuery (name="CDBean.findByTitleNative",
query="select * from CDBean  where cdtitle like ?",
resultClass=CDBean.class)
,
@NamedNativeQuery (name="CDBean.findAllSorted",
query="select * from CDBean order by cdtitle ASC,cdprice DESC",
resultClass=CDBean.class)
}
)
@Table(name="cdbean")
public class CDBean {

	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CD_SEQ")
    @SequenceGenerator(sequenceName = "cd_seq", allocationSize = 1, name = "CD_SEQ")
	private  int cdid;
	
	private  String cdtitle;
	 
	private Float cdprice;
	 
	private String cdpublisher;

	public CDBean() {
		super();
	}
	
	public CDBean(int cdid, String cdtitle, Float cdprice, String cdpublisher) {
		super();
		this.cdid = cdid;
		this.cdtitle = cdtitle;
		this.cdprice = cdprice;
		this.cdpublisher = cdpublisher;
	}

	public int getId() {
		return cdid;
	}

	public void setId(int cdid) {
		this.cdid = cdid;
	}

	public String getCdtitle() {
		return cdtitle;
	}

	public void setCdtitle(String cdtitle) {
		this.cdtitle = cdtitle;
	}

	public Float getCdprice() {
		return cdprice;
	}

	public void setCdprice(Float cdprice) {
		this.cdprice = cdprice;
	}

	public String getCdpublisher() {
		return cdpublisher;
	}

	public void setCdpublisher(String cdpublisher) {
		this.cdpublisher = cdpublisher;
	}

	@Override
	public String toString() {
		return "CDBean [cdid=" + cdid + ", cdtitle=" + cdtitle + ", cdprice=" + cdprice + ", cdpublisher=" + cdpublisher
				+ "]";
	}

}
