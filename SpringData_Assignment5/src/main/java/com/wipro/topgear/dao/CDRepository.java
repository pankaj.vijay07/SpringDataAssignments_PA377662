package com.wipro.topgear.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.wipro.topgear.model.CDBean;

@Repository
public interface CDRepository extends CrudRepository<CDBean, Integer> {

	List<CDBean> findByCdprice(@Param("cdprice")float cdprice);
	
	List<Object[]> numberOfCDs();
	
}
