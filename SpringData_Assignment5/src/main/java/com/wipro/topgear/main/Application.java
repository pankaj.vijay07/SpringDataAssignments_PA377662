package com.wipro.topgear.main;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.wipro.topgear.dao.CDRepository;
import com.wipro.topgear.model.CDBean;
/*

5.	Rewrite the solution of assignment(2) with the below updates/additions:
a.	Add a �Named Query� called �findByCdPrice� which should show all the CDs which have a price greater than the given price.
b.	The above query should also show the �No. of CDs� as per the price criteria 
(Hint: Implement this as a Named Query i.e. @NamedQuery)




*/

@SpringBootApplication
@EnableJpaRepositories("com.wipro.topgear.dao")
@EntityScan(basePackages = "com.wipro.topgear.model")
public class Application implements CommandLineRunner {

	@Autowired
	DataSource dataSource;

	@Autowired
	CDRepository cdRepository;

	public static void main(String[] args) throws Exception {
		SpringApplication.run(Application.class, args);
	}

	@Transactional(readOnly = true)
	public void run(String... args) throws Exception {

		System.out.println("DATASOURCE = " + dataSource);

		// to insert a new CD in the database
		CDBean cdBean = new CDBean(1, "Little Willie Blues", 786.0f, "Jaboo Smith");
		saveCDDetails(cdBean);

		findCdByPrice(786.0f);

		numberOfCDs();

		System.out.println("Successfully executed all the operations!");

	}

	private void saveCDDetails(CDBean bean) {

		System.out.println("Saving the record.............");
		CDBean cdbean = cdRepository.save(bean);
		if (cdbean == null) {
			System.out.println("Failed to save the record");
		} else {
			System.out.println("Record saved successfully");
		}
	}

	private void findCdByPrice(float cdprice) {

		System.out.println("fetching the record..........");
		List<CDBean> list = cdRepository.findByCdprice(cdprice);
		for (CDBean bean : list) {
			System.out.println("CD Details as per the cdprice:" + bean);
		}

	}

	private void numberOfCDs() {

		List<Object[]> list = cdRepository.numberOfCDs();
		for (Object obj : list) {
			System.out.println("Count of the total number of cd's are" + obj);
		}

	}

}
